<?php

namespace Corp\Http\Controllers;

use Corp\Repositories\MenusRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Menu;

class SiteController extends Controller
{
    //
    protected $p_rep;
    protected $s_rep;
    protected $a_rep;
    protected $m_rep;


    protected $keywords;
    protected $meta_desc;
    protected $title;

    protected $template;

    protected $vars = array();

    protected $contentRightBar = FALSE;
    protected $contentLeftBar = FALSE;
    protected $bar = 'no';

    public function __construct(MenusRepository $m_rep){
        $this->m_rep = $m_rep;
    }

    protected function renderOutput(){


        $menu = $this->getMenu();


        $navigation = view(env('THEME') . '.navigation')->with('menu', $menu)->render();
        $this->vars = Arr::Add($this->vars, 'navigation', $navigation);
        $this->vars = Arr::Add($this->vars, 'bar', $this->bar);
        $this->vars = Arr::Add($this->vars, 'keywords', $this->keywords);
        $this->vars = Arr::Add($this->vars, 'meta_desc', $this->meta_desc);
        $this->vars = Arr::Add($this->vars, 'title', $this->title);


        if($this->contentRightBar){
            $rightBar = view(env('THEME') . '.rightBar')->with('contentRightBar', $this->contentRightBar)->render();
            $this->vars = Arr::Add($this->vars, 'rightBar', $rightBar);
        }

        if($this->contentLeftBar){
            $leftBar = view(env('THEME') . '.leftBar')->with('contentLeftBar', $this->contentLeftBar)->render();
            $this->vars = Arr::Add($this->vars, 'leftBar', $leftBar);
        }

        $footer = view(env('THEME') . '.footer')->render();
        $this->vars = Arr::Add($this->vars, 'footer', $footer);

        return view($this->template)->with($this->vars);
    }

    protected function getMenu()
    {
        $menu = $this->m_rep->get();

       $mBuilder = Menu::make('MyNav', function($m) use ($menu){
           foreach ($menu as $item) {
               if( $item->parent == 0){
                  $m->add($item->title, $item->path)->id($item->id);
               }
               else{
                   if($m->find($item->parent)){

                       $m->find($item->parent)->add($item->title, $item->path)->id($item->id);
                   }
               }
            }
       });

//       dd($mBuilder);

        return $mBuilder;
    }

}
