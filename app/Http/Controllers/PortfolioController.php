<?php

namespace Corp\Http\Controllers;

use Corp\Repositories\PortfoliosRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class PortfolioController extends SiteController
{
    //
    public function __construct(PortfoliosRepository $p_rep)
    {
        parent::__construct(new \Corp\Repositories\MenusRepository(new \Corp\Menu));

        $this->p_rep = $p_rep;

        $this->template = env('THEME') . '.portfolios';

    }

    public function index()
    {

        $this->title = 'Портфолио';
        $this->keywords = 'Портфолио';
        $this->meta_desc = 'Портфолио';

        $portfolios = $this->getPortfolios();

        $content = view(env('THEME') . '.portfolios_content')->with('portfolios', $portfolios)->render();

        $this->vars = Arr::Add($this->vars, 'content', $content);
        $this->vars = Arr::Add($this->vars, 'keywords', $this->keywords);
        $this->vars = Arr::Add($this->vars, 'meta_desc', $this->meta_desc);
        $this->vars = Arr::Add($this->vars, 'title', $this->title);
//        dd($portfolios);
        return $this->renderOutput();
    }

    public function getPortfolios($take = FALSE, $paginate = TRUE){
        $portfolios =  $this->p_rep->get('*', FALSE, $paginate);
        if($portfolios){
            $portfolios->load('filter');
        }
        return $portfolios;
    }



    public function show($alias = FALSE)
    {


        $portfolio = $this->p_rep->one($alias);

        $this->title = $portfolio->title;
        $this->keywords = $portfolio->keywords;
        $this->meta_desc = $portfolio->meta_desc;
        $portfolios = $this->getPortfolios(config('settings.other_portfolios'), FALSE);

        $content = view(env('THEME') . '.portfolio_content')->with(['portfolio'=> $portfolio, 'portfolios' => $portfolios])->render();
        $this->vars = Arr::Add($this->vars, 'content', $content);



//        dd($portfolios);

//        $comments = $this->getComments(config('settings.recent_comments'));
//        $portfolios = $this->getPortfolios(config('settings.recent_portfolios'));

//        dd($portfolios);
//        $this->contentRightBar = view(env('THEME') . '.articlesBar')->with(['comments'=>$comments, 'portfolios'=>$portfolios]);
//        $this->vars = Arr::Add($this->vars, 'bar', $this->bar);
//        $this->vars = Arr::Add($this->vars, 'keywords', $this->keywords);
//        $this->vars = Arr::Add($this->vars, 'meta_desc', $this->meta_desc);
//        $this->vars = Arr::Add($this->vars, 'title', $this->title);

        return $this->renderOutput();
    }

}
