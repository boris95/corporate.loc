 <li id="li-comment-{{ $data['id'] }}" class="comment even borGreen}">
        <div id="comment-{{ $data['id'] }}" class="comment-container">
            <div class="comment-author vcard">
                <img alt="" src="https://www.gravatar.com/avatar/{{ $data['hash'] }}?d=mm&75" class="avatar" height="75" width="75" />
                <cite class="fn">{{ $data['name'] }}</cite>
            </div>
            <!-- .comment-author .vcard -->
            <div class="comment-meta commentmetadata">
                <div class="intro">
                    <div class="commentDate">
                        <a href="#comment-2">
                            September 24, 2012 at 1:31 pm</a>
                    </div>
                    <div class="commentNumber">#&nbsp;1</div>
                </div>
                <div class="comment-body">
                    <p>{{ $data['text'] }}</p>
                </div>
                <!-- .reply -->
            </div>
            <!-- .comment-meta .commentmetadata -->
        </div>
    </li>
